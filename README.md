# Simulator of deterministic pushdown automaton

Simulator of deterministic pushdown automaton (DPA). Implemented in Java.

My lab assignment in Introduction to Theoretical Computer Science, FER, Zagreb.

Created: 2019
