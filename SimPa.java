import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class SimPa {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		String input = reader.readLine();
		String states = reader.readLine();
		String alphabet = reader.readLine();
		String stackAlphabet = reader.readLine();
		String acceptableStates = reader.readLine();
		String initialState = reader.readLine();
		String stackInitialChar = reader.readLine();

		List<String> transitions = new ArrayList<>();

		String t = reader.readLine();
		while (t != null && !t.trim().isEmpty()) {
			transitions.add(t);
			t = reader.readLine();
		}

		String[] inputsI = input.split("\\|");
		String[] statesQ = states.split(",");
		String[] alphabetS = alphabet.split(",");
		String[] stackAlphabetS = stackAlphabet.split(",");
		String[] acceptableStatesQ = acceptableStates.split(",");
		String initialStateQ = initialState;
		String stackInitialCharQ = stackInitialChar;

		if (acceptableStatesQ.length == 1 && acceptableStatesQ[0].isEmpty())
			acceptableStatesQ = new String[0];

		List<String> acceptables = Arrays.asList(acceptableStatesQ);
		
		
		Map<TableRowLeft,TableRowRight> rows = makeTable(transitions, acceptables);
		
		List<String> inputsPrint = new ArrayList<>(inputsI.length);
		
	
		for (String inp : inputsI) {
			String currentState = initialStateQ;
			String currentStack = stackInitialChar;
	
			String[] inChs = inp.split(",");
			
			String printStr = currentState+"#"+currentStack+"|";
			
			boolean unexistingTransition = false;
			
			for (String inCh : inChs) {
				
				TableRowRight newStateStack = rows.get(new TableRowLeft(currentState, inCh, ""+currentStack.charAt(0)));
				
				TableRowRight transitionEps = null;
				if (newStateStack == null) {
					TableRowRight rightEps = rows.get(new TableRowLeft(currentState, "$", ""+currentStack.charAt(0)));
					if (rightEps == null || currentStack.equals("$")) {
						unexistingTransition = true;
						break;
					}
					while (rightEps != null) {
						transitionEps = rightEps;
						currentState = rightEps.nextState;
						currentStack = rightEps.stackChars+currentStack.substring(1);
						printStr += currentState + "#" + currentStack + "|";
						
						Map<TableRowLeft,TableRowRight> rowsTmp = new HashMap<>(); 
						for (Map.Entry<TableRowLeft,TableRowRight> entry : rows.entrySet()) {
							if (entry.getKey().currentState.equals(currentState) && entry.getKey().inStackChar.equals(""+currentStack.charAt(0))) {
								rowsTmp.put(entry.getKey(), entry.getValue());
							}
						}

						rightEps = rows.get(new TableRowLeft(currentState, "$", ""+currentStack.charAt(0)));
					}
				}
				
				if (rows.get(new TableRowLeft(currentState, inCh, ""+currentStack.charAt(0))) == null || currentStack.equals("$")) {
					unexistingTransition = true;
					break;
				}
				
				if (transitionEps != null) {
					newStateStack = rows.get(new TableRowLeft(currentState, inCh, ""+currentStack.charAt(0)));
				}
				currentState = newStateStack.nextState;
				currentStack = newStateStack.stackChars+currentStack.substring(1);
				
				if (newStateStack.stackChars.equals("$") && currentStack.length()>1) {
					currentStack = currentStack.substring(1);
				}
				printStr += currentState + "#" + currentStack + "|";
				
			}
		
			
			
			if (!acceptables.contains(currentState)) {
				TableRowRight epsTrans = rows.get(new TableRowLeft(currentState, "$", ""+currentStack.charAt(0)));
				while (epsTrans != null && !acceptables.contains(currentState)) {
					currentState = epsTrans.nextState;
					currentStack = epsTrans.stackChars+currentStack.substring(1);
					printStr += currentState + "#" + currentStack + "|";
					epsTrans = rows.get(new TableRowLeft(currentState, "$", ""+currentStack.charAt(0)));
				}
			}
			
			boolean isAcceptable = false;
			
			if (unexistingTransition) {
				printStr += "fail|";
			} else {
				if (acceptables.contains(currentState)) {
					isAcceptable = true;
				}
			}
			
			
			printStr += isAcceptable ? "1" : "0";
			
			inputsPrint.add(printStr);
		}
		
		inputsPrint.forEach(System.out::println);
		

		reader.close();
	}

	private static Map<TableRowLeft,TableRowRight> makeTable(List<String> transitions, List<String> acceptables) {
		Map<TableRowLeft,TableRowRight> tableRows = new LinkedHashMap<>();
		for (String t : transitions) {
			String[] lr = t.split("->");
			String[] l = lr[0].split(",");
			String currentQ = l[0];
			String currentCh = l[1];
			String currentStackCh = l[2];
			String[] r = lr[1].split(",");
			String newState = r[0];
			String stackCharsArr = r[1];

			tableRows.put(new TableRowLeft(currentQ, currentCh, currentStackCh), new TableRowRight(newState, stackCharsArr));
		}

		return tableRows;

	}

	private static class TableRowLeft {
		private String currentState;
		private String inChar;
		private String inStackChar;
		
		public TableRowLeft(String currentState, String inChar, String inStackChar) {
			super();
			this.currentState = currentState;
			this.inChar = inChar;
			this.inStackChar = inStackChar;
		}

		public String getCurrentState() {
			return currentState;
		}

		public String getInChar() {
			return inChar;
		}

		public String getInStackChar() {
			return inStackChar;
		}

		@Override
		public int hashCode() {
			return Objects.hash(currentState, inChar, inStackChar);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof TableRowLeft))
				return false;
			TableRowLeft other = (TableRowLeft) obj;
			return Objects.equals(currentState, other.currentState) && Objects.equals(inChar, other.inChar)
					&& Objects.equals(inStackChar, other.inStackChar);
		}

		@Override
		public String toString() {
			return currentState + "," + inChar + "," + inStackChar;
		}
	}
	
	private static class TableRowRight {
		private String nextState;
		private String stackChars;
		
		public TableRowRight(String nextState, String stackChars) {
			super();
			this.nextState = nextState;
			this.stackChars = stackChars;
		}
		public String getNextState() {
			return nextState;
		}

		public String getStackChars() {
			return stackChars;
		}



		@Override
		public int hashCode() {
			return Objects.hash(nextState, stackChars);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof TableRowRight))
				return false;
			TableRowRight other = (TableRowRight) obj;
			return  Objects.equals(nextState, other.nextState)
					&& Objects.equals(stackChars, other.stackChars);
		}

		@Override
		public String toString() {
			return nextState + "," + stackChars;
		}
	}

}
